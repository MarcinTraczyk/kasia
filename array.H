#include "container.H"

class array{
  private:
    int dimX_;
    int dimY_;
    container* boxes_;

  public:
    array(int nx, int ny);

    container& getElement(int nx, int ny);

    void setContainer(int nx, int ny, float x);
};

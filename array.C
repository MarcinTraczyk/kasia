#include "array.H"

array::array(int nx, int ny){
  dimX_ = nx;
  dimY_ = ny;
  boxes_ = new container[dimX_ * dimY_];
}

container& array::getElement(int nx, int ny){
  return boxes_[nx * dimX_ + ny]; 
}

void array::setContainer(int nx, int ny, float x){
  boxes_[nx * dimX_ + ny].setX(x);
}
